#include "stdio.h"


typedef enum { False, True } bool;

bool isEndian(void)
{
	int val = 0x12345678L;

	if (val&0xFFFF == 0x5678)
		return True;
	else
		return False;
}

int main(int argc, char **argv)
{

	bool result;
	result = isEndian();
	result ? printf("big endian\n") : printf("little endian\n");
	
	return 0;
}
